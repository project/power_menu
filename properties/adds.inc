<?php

/**
 * @file
 */

/**
 * Implementation of hook_power_menu_properties().
 */
function adds_power_menu_properties() {
  return array(
    'adds_skyscraper' => array(
      'file' => 'adds.inc',
      'path' => drupal_get_path('module', 'power_menu') . '/properties',
      'admin_submit' => 'adds_power_menu_admin_submit', //callback when submitting the menu item edit
      'property_name' => 'adds_skyscraper', // name of the property
      'property_load' => 'adds_skyscraper_power_menu_load_property', // callback for loading the property
      'description' => 'adds add, that is being outputted as variable to the page.tpl.php',
      'scope' => 'page', //options: page
    ),
    'adds_leaderboard' => array(
      'file' => 'adds.inc',
      'path' => drupal_get_path('module', 'power_menu') . '/properties',
      'admin_submit' => 'adds_power_menu_admin_submit', //callback when submitting the menu item edit
      'property_name' => 'adds_leaderboard', // name of the property
      'property_load' => 'adds_leaderboard_power_menu_load_property', // callback for loading the property
      'description' => 'adds add, that is being outputted as variable to the page.tpl.php',
      'scope' => 'page', //options: page
    ),
    'adds_rectangle' => array(
      'file' => 'adds.inc',
      'path' => drupal_get_path('module', 'power_menu') . '/properties',
      'admin_submit' => 'adds_power_menu_admin_submit', //callback when submitting the menu item edit
      'property_name' => 'adds_rectangle', // name of the property
      'property_load' => 'adds_rectangle_power_menu_load_property', // callback for loading the property
      'description' => 'adds add, that is being outputted as variable to the page.tpl.php',
      'scope' => 'block', //options: page
      'title' => t('Add'), // optional, only used for block scope
    ),
  );
}

/**
 * Callback function for the skyscraper property
 * @param $mlid
 * @param $admin
 */
function adds_skyscraper_power_menu_load_property($mlid, $admin = FALSE) {
  return _adds_power_menu_load_property($mlid, $admin, 'adds_skyscraper');
}

/**
 * Callback function for the leaderboard property
 * @param $mlid
 * @param $admin
 */
function adds_leaderboard_power_menu_load_property($mlid, $admin = FALSE) {
  return _adds_power_menu_load_property($mlid, $admin, 'adds_leaderboard');
}

/**
 * Callback function for the rectangle property
 * @param $mlid
 * @param $admin
 */
function adds_rectangle_power_menu_load_property($mlid, $admin = FALSE) {
  return _adds_power_menu_load_property($mlid, $admin, 'adds_rectangle');
}

function _adds_power_menu_load_property($mlid, $admin = FALSE, $type) {
  if (is_object($mlid)) {
    $mlid = $mlid->mlid;
  }

  $res = db_fetch_object(db_query("SELECT pmp.value, ml.plid FROM {power_menu_properties} pmp LEFT JOIN {menu_links} ml ON pmp.mlid = ml.mlid WHERE pmp.mlid=%d AND pmp.property_name='%s'", $mlid, $type));
  if ($res->value == '' && $res->plid > 0 && !$admin) {
    $res = adds_rectangle_power_menu_load_property($res->plid, $admin, $type);
  }
  elseif ($admin) {
    return $res->value;
  }
  elseif ($res->value == '') {
    $res = db_fetch_object(db_query("SELECT pmp.value FROM {power_menu_properties} pmp WHERE pmp.mlid=%d AND pmp.property_name='%s'", 0, $type));
  }
  return drupal_eval($res->value);
}

/**
 * Callback function when submitting a menu item... storing the
 * properties
 * @param $form
 * @param $form_state
 */
function adds_power_menu_admin_submit($form, &$form_state) {
  if (!$mlid = check_plain($form_state['values']['menu']['mlid'])) {
    $mlid = 0;
  }
  $props = adds_power_menu_properties();
  db_query("DELETE FROM {power_menu_properties} WHERE mlid = %d", $mlid);

  foreach ($props as $prop) {
    $db = array(
      'mlid' => $mlid,
      'property_name' => $prop['property_name'],
      'value' => $form_state['values']['properties'][$prop['property_name']],
    );
    drupal_write_record('power_menu_properties', $db);
  }
}