
=============================
 API
=============================

This Modul provides the following hooks for other modules to interact:

hook_power_menu_href
=============================
Let's assume you have the following situation. In your module you define
via hook_menu a path (say, /my_path/%). You implement the callback in 
the following way:

my_path -> display some kind of overview
my_path/23 -> only show the kinds of 23

In your menu you link to my_path/all. So going over the menu to path/all
all is well, your menu item get's activated, but then you go to element
23, my_path/all won't be activated anymore in your menu. Bummer.

my_path/23 is not of a nodetype kind and you can't assign a taxonomy
term to it. But not all hope is lost.

hook_power_menu_href will save you. It is being called on every page 
load and sending you the currently active menu item (which might not
be the one you would like to be activated). You can return a href of
a menu item that should be activated.

Example:

function rapsli_power_menu_href($menu_item) {
  $ar_active = array('my_path' => 'my_path/*');
  foreach ($ar_active as $key => $pattern) {
    if (drupal_match_path($menu_item['href'], $pattern)) {
      return $key;
    }
  }
}

With the array ar_active we defined a pattern and a menu href. So you 
could read this array as followed: If we are on a page with the pattern
my_path/* activate the menu item with the path "my_path".
We loop through the array and if we have a match of our pattern with the
current href, we return the desired href.

That's it.

hook_power_menu_properties
=============================
Define properties on your menu items and use a cool inheritance concept. So
for example lets say you want to display different adds, based on your menu.
But there might be pages with no special adds, resp. they have the same add
as the parent item. For example you have a menu item "sport" that has child
elements "hockey" and "tennis". You want the same adds on all three pages.
So you would set a property on sport, which is then also used by hockey and
tennis. In the adds.inc there is the exxample hook implementation:

function adds_power_menu_properties() {
  return array(
    'adds_skyscraper' => array(
      'file' => 'adds.inc',
      'path' => drupal_get_path('module', 'power_menu') . '/properties',
      'admin_submit' => 'adds_power_menu_admin_submit', //callback when submitting the menu item edit
      'property_name' => 'adds_skyscraper', // name of the property
      'property_load' => 'adds_skyscraper_power_menu_load_property', // callback for loading the property
      'description' => 'adds add, that is being outputted as variable to the page.tpl.php',
      'scope' => 'page', //options: page, block
    ),
    'adds_leaderboard' => array(
      'file' => 'adds.inc',
      'path' => drupal_get_path('module', 'power_menu') . '/properties',
      'admin_submit' => 'adds_power_menu_admin_submit', //callback when submitting the menu item edit
      'property_name' => 'adds_leaderboard', // name of the property
      'property_load' => 'adds_leaderboard_power_menu_load_property', // callback for loading the property
      'description' => 'adds add, that is being outputted as variable to the page.tpl.php',
      'scope' => 'page', //options: page, block
    ),
    'adds_rectangle' => array(
      'file' => 'adds.inc',
      'path' => drupal_get_path('module', 'power_menu') . '/properties',
      'admin_submit' => 'adds_power_menu_admin_submit', //callback when submitting the menu item edit
      'property_name' => 'adds_rectangle', // name of the property
      'property_load' => 'adds_rectangle_power_menu_load_property', // callback for loading the property
      'description' => 'adds add, that is being outputted as variable to the page.tpl.php',
      'scope' => 'block', //options: page, block
      'title' => t('Add'), // optional, only used for block scope
    ),
  );
}