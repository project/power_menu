/* $Id: */

-- SUMMARY --

Ok. So what does this module do? Let's say you have a primary menu: sports
politics, people and riddles. Sports, people and politics link to the same
view and just pass an argument (the taxonomy term). So you have a vocabulary
that has three terms: sports, politics and people. The riddles section is 
an other view that just lists all nodes of the type "riddles" (remember, 
the other view would list articles filtered by a taxonomy argument).

So now comes the tricky part. You click on sports. This menu item is going
to be marked as active. You now click on a node and would actually expect
the sports menu still be marked active... wrong. So this is what we want
to solve.

Enable the Power Menu. Go to the settings and configure which of your
vocabularies is being represented in you primary menu. Now you just go
to your primary menu, and go to your sports item. You'll see that you have
more options. You can create an alias! But then you have three new options:

+ Create taxonomy term.
This would be used when we want to create a new menu item. It is then going
to create a new taxonomy term into our vocabulary and link it to it.

+ Link to existing term
+ Active when node of type... is displayed

You can mark them. Every time you look at a full node (for example node/234)
and it is either of type xyz or belongs to the given taxonomy this menu item
is going to be marked active (or active-trail).

Well... that's basically it. Any improvements on this readme are VERY welcome


-- MAINTAINERS --

rapsli - http://drupal.org/user/107403


-- INSPIRATION --

menutrails - http://drupal.org/project/menutrails


-- API --

Please have a look at API.txt test


-- Common mistake --
- Power Menu does not work. Go to the power menu configuration page. Only ONE
  Power Menu must be active at the same time on a page, else it's going to 
  screw up.