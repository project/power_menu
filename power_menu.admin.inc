<?php

/**
 * @file
 * This contains all the admin stuff of the module
 */

/**
 * Callback function for the admin form
 */
function power_menu_admin_form() {
  global $language;

  $menus = menu_get_menus();
  
  $form['power_menu_menu'] = array(
    '#type' => 'checkboxes',
    '#options' => $menus,
    '#title' => t('Power Menu'),
    '#default_value' => variable_get('power_menu_menu', array()),
    '#description' => t('Choose the menu which should be your power menu. You can choose multiple menus. Main usecase for selecting multiple menus
    would be if you have a site with multiple languages. IMPORTANT: At the same time only one menu should be present!!!'),
  );

  $form['power_menu_breadcrumb_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display title in breadcrumbs'),
    '#default_value' => variable_get('power_menu_breadcrumb_title', FALSE),
  );

  if (module_exists('taxonomy')) {
    $ar_vocabs = array();
    $vocabs = taxonomy_get_vocabularies();
    foreach ($vocabs as $vocab) {
      $ar_vocabs[$vocab->vid] = $vocab->name;
    }

    $form['power_menu_taxonomy_navigation'] = array(
      '#type' => 'select',
      '#options' => $ar_vocabs,
      '#title' => t('Navigation Taxonomy'),
      '#default_value' => variable_get('power_menu_taxonomy_navigation', ''),
      '#description' => t('Choose the taxonomy which is going to reflect the structure of your menu. It gives you the option to create taxonomy terms from your menu.'),
    );
  }
  else {
    drupal_set_message(t("You don't have the taxonomy enabled. There is nothing to configure."));
  }

  return system_settings_form($form);
}