; $Id:

Changes
===============
2010/10/25
* fixing bug, when tid=0, nothing should happen
* fixing bug with the properties. They were not quite working but should now

2010/09/16
* Removed some E_NOTICE errors
* fixing problem when node is directly in the menu

2010/09/02
* adding properties feature

2010/07/30
* Possibility to turn on/off adding title to breadcrumbs
* Fix in token, when using as path

* Added support for multi language site (multiple blocks)
* Make sure that you call power_menu_get_menu() to get the power_menu!!!!!!
* Panels support

2010/05/19
* added hook_power_menu_href
* some improvements on taxonomy recognition
* better documentation

2010/05/19
* Possibility to select a "Power Menu"